package ro.aniela;

import java.security.PublicKey;
import java.util.Objects;

public class Country {
    private long population;
    private String name ;
    private GovernmentForm governmentForm;

    public Country(long population, String name, GovernmentForm governmentForm){
        this.governmentForm=governmentForm;
        this.name=name;
        this.population=population;

    }

    public long getPopulation (){
        return population;

    }
    public String getName(){
        return name;

    }

    public GovernmentForm getGovernmentForm(){
        return governmentForm;

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Country country = (Country) o;
        return population == country.population && Objects.equals(name, country.name) && governmentForm == country.governmentForm;
    }

    @Override
    public int hashCode() {
        return Objects.hash(population, name, governmentForm);
    }
}
