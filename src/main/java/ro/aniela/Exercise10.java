package ro.aniela;

import java.util.List;

public class Exercise10 {
    public static void main(String[]args){

        List<Integer> naturalNumbers= List.of(1,2,3,4,5);
        for(Integer i:naturalNumbers){
            if(i%2==0){
                System.out.println(i);
            }
        }

    }


}
