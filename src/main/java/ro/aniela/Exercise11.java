package ro.aniela;

import java.util.List;

public class Exercise11 {
    public static void main(String[] args) {

        List<String> unu = List.of("1", "2", "3", "4", "5");
        List<String> doi = List.of("1", "2", "3", "4", "5");
        System.out.println(areEquals(unu, doi, 0));

    }

    private static boolean areEquals(List<String> one, List<String> two, int index) {
        return one.get(index).equals(two.get(index));
    }

}
