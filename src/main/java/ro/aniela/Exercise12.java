package ro.aniela;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Exercise12 {

    public static void main(String[] args) {
        List<Integer> list = List.of(1, 2, 3,9, 4, 5, 6, 7);
        System.out.println(extremeNumber(list));
        List<Integer> list2=new ArrayList<>();
        list2.addAll(list);
        System.out.println(extremeNumbers(list2));

    }

    private static Pair extremeNumber(List<Integer> numbers) {
        int largest = 0;
        int smallest = 0;
        for (Integer j : numbers) {
            if (j > largest) {
                largest = j;
            }
            if (j < smallest) {
                smallest = j;
            }

        }
        return new Pair(largest, smallest);
    }

    private static Pair extremeNumbers(List<Integer> numbers) {
        Collections.sort(numbers);
        int largest = numbers.get(numbers.size() - 1);
        int smallest = numbers.get(0);
        return new Pair(largest, smallest);
    }
}
