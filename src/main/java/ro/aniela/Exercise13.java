package ro.aniela;

import java.util.Map;

public class Exercise13 {
    public static void main(String[] args) {
        Map<Integer, String> map = Map.of(1, "abs", 2, "avs", 5, "bla");
        System.out.println(valuesToUppercase(map));

    }

    private static String valuesToUppercase(Map<Integer, String> map) {
        String values = "";
        for (String s : map.values()) {
            values += s.toUpperCase() + ", ";
        }
        return values.substring(0, values.length() - 2);
    }

    private static String valuesToUppercase2(Map<Integer, String> map) {
        StringBuilder sb = new StringBuilder();
        for (String s : map.values()) {
            sb.append(s.toUpperCase())
                    .append(", ");

        }
        String values=sb.toString();
        return values.substring(0, values.length() - 2);
    }

}
