package ro.aniela;

import java.util.Map;

public class Exercise14 {
    public static void main(String[] args) {
        Map<Integer, String> mappp = Map.of(1, "123", 2, "abs");
        System.out.println(keys(mappp));

    }

    private static String keys(Map<Integer, String> map) {
        StringBuilder sb = new StringBuilder();
        for (Integer key : map.keySet()) {
            sb.append(key).append(", ");
        }
        return sb.substring(0,sb.length()-2);
    }
}
