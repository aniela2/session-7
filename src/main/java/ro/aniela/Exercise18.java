package ro.aniela;

import java.util.Map;

public class Exercise18 {
    public static void main(String[] args) {
        Map<String, Integer> map = Map.of("one", 1, "te", 2, "three", 3);
        System.out.println(sumOfValues(map));
    }

    private static int sumOfValues(Map<String, Integer> map) {
        int sum = 0;
        for (Integer i : map.values()) {
            sum += i;
        }
        return sum;
    }


}
