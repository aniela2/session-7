package ro.aniela;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Exercise19 {
    public static void main(String[] args) {
        List<String> strigs = List.of("one", "two", "jd", "jsfh");
        System.out.println(theModifiedMap(strigs));


    }

    private static Map<Integer, String> theModifiedMap(List<String> strings) {
        Map<Integer, String> map = new HashMap<>();
        for (int i = 0; i < strings.size(); i++) {
            map.put(i, strings.get(i));
        }
        return map;
    }
}
//Having a List of Strings, write a Map of natural numbers and String.
// The keys should be incremented by 1(one) and the values are the elements from the given List.