package ro.aniela;

import java.util.List;
import java.util.Map;

public class Exercise21 {
    public static void main(String[] args) {

        Map<Integer, List<String>> map = Map.of(1, List.of("banana", "baneta", "bla"),
                2, List.of("banal", "bist"),
                3, List.of("blablabla"));

        System.out.println(prefix(map, "ban"));

    }


    private static String prefix(Map<Integer, List<String>> map, String prefix) {
        StringBuilder sb = new StringBuilder();
        for (List<String> l : map.values()) {
            for (String s : l)
                if (s.startsWith(prefix)) {
                    sb.append(s).append(", ");
                }
        }
        return sb.substring(0,sb.length()-2);
    }

}
