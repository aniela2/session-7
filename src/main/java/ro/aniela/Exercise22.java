package ro.aniela;

import java.util.HashMap;
import java.util.Map;

public class Exercise22 {
    public static void main(String[] args) {
        System.out.println(buildifAMap("I have a motorcycle"));

    }

    private static Map<Integer, String> buildifAMap(String phrase) {
        Map<Integer, String> map = new HashMap<>();
        int i = 0;
        for (String s : phrase.split(" ")) {
            map.put(++i, s);
        }
        return map;
    }
}
