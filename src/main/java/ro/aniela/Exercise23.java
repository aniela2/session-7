package ro.aniela;

import java.util.Map;

public class Exercise23 {

    public static void main(String[] args) {
        Tuple t = new Tuple("Oana", "MAria");
        Tuple p = new Tuple("Ciucanu", "adrian");
        User u = new User(t, 23, "Brasov");
        User v = new User(p, 34, "Ploiesti");
        Map<Integer, User> m = Map.of(1, u, 2, v);
        System.out.println(averageAge(m));


    }

    private static double averageAge(Map<Integer, User> map) {

        double sum = 0;
        for (User u : map.values()) {
            sum += (double) u.getAge();
        }
        return sum / (double)map.size();
    }

}
