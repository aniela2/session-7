package ro.aniela;

public class Exercise4 {
    public static void main(String[]args){

        Person p= new Person("Ion", 25, Gender.MALE);
        System.out.println(p);
        Person j= new Person("vali", 34, Gender.NONE);

        System.out.println(j);

    }
}
//Write a Person class with the following attributes: name, age, gender(enum).
// At the creation of a Person object the values permitted for gender
// are male or female otherwise an exception will be thrown.
// Create two objects from the Person class and print them into the console.