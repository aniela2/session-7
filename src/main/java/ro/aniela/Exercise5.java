package ro.aniela;

import java.util.List;
import java.util.WeakHashMap;

//Write a class Wine with the following attributes: type(enum), alcoholPercentage, country, bottleSize.
// Add 5 objects instantiated from Wine class to a List.
//Write methods to:
//display into the console only the red type wines;
//display into the console the average of alcoholPercentage;
//display into the console all the countries in uppercase.

public class Exercise5 {
    public static void main(String[] ags) {
        Wine w = new Wine(Type.RED, 12.5, "rOM", 2.0);
        Wine p = new Wine(Type.ROSE, 12d, "en", 2);
        Wine wine = new Wine(Type.WHITE, 12d, "e", 12);

        List<Wine> wines = List.of(w, p, wine);
        wineByType(wines, Type.RED);
      System.out.println(average(wines));
      upperCase(wines);

    }

    private static void wineByType(List<Wine> wines, Type t) {
        for (Wine w : wines) {
            if (w.getType() == t) {
                System.out.println(w);
            }
        }
    }
    private static double average(List<Wine> list){
        double sum=0d;
        for(Wine w: list){
          sum= sum+w.getAlcoholPercentage();
        }
        return sum/list.size();
    }
    private static void upperCase(List<Wine> wines){
        for(Wine l: wines){
          System.out.println(l.getCountry().toUpperCase());
        }
    }
}
