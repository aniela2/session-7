package ro.aniela;

import com.sun.source.tree.BreakTree;

import java.util.Set;

public class Exercise6 {
    public static void main(String[] args) {

        Country ro = new Country(12_234_455, "Anglia", GovernmentForm.REPUBLIC);
        Country c = new Country(123_445_555, "name", GovernmentForm.MONARCHY);
        Country en = new Country(1234455, "en ", GovernmentForm.REPUBLIC);
        Set<Country> countries = Set.of(ro, c, en);
        System.out.println(totalPopulation(countries));
        System.out.println(averagePopulation(countries));
        startsWith(countries);
    }

    private static long totalPopulation(Set<Country> countries) {
        long sum = 0;
        for (Country c : countries) {
            sum += c.getPopulation();
        }
        return sum;
    }

    private static long averagePopulation(Set<Country> countries) {
        return totalPopulation(countries) / countries.size();
    }

    private static void startsWith(Set<Country> countries){
        for(Country c:countries){
           if( (c.getName().startsWith("A") || c.getName().startsWith("B") )&& c.getGovernmentForm()!=GovernmentForm.MONARCHY){
               System.out.println(c);
           }
        }
    }


}
