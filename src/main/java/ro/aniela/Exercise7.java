package ro.aniela;

import java.util.ArrayList;
import java.util.List;

public class Exercise7 {
    public static void main(String[] args) {
        String[] colors={"bana", "cana","buna"};
        System.out.println(colors(colors));

    }

    private static List<String> colors(String[] colors) {
        List<String> filteredColors = new ArrayList<>();
        for (String s : colors) {
            if (s.startsWith("b")) {
                filteredColors.add(s);
            }
        }
        return filteredColors;
    }
}
