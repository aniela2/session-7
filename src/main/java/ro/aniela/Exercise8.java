package ro.aniela;

import java.util.List;

public class Exercise8 {
    public static void main(String[] args) {
        List<Integer> numbers= List.of(1,2,3);
        System.out.println(numbers.contains(2));
        System.out.println(presentInListOrNot(numbers, 2));

    }

    private static boolean presentInListOrNot(List<Integer> numbers, int element) {
       return numbers.contains(element);

    }
}
