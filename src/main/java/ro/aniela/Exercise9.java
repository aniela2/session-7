package ro.aniela;

import java.util.List;

public class Exercise9 {
    public static void main(String []args){

        Animal a = new Carnivore();
        Animal b= new Carnivore();
        Animal c= new Herbivore();
        Animal d= new Herbivore();
        List<Animal> animals= List.of(a,b,c,d);
        for(Animal x: animals){
            if(x instanceof Carnivore){
                System.out.println(x);
            }
        }

    }

}
