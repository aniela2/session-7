package ro.aniela;

public class Pair {
    private int largest;
    private int smallest;

    public Pair(int largest, int smallest){
        this.largest=largest;
        this.smallest=smallest;
    }

    public int getLargest(){
        return largest;
    }
    public int getSmallest(){
        return smallest;
    }
    public String toString(){
        return "Pair : largest " + largest + ", smallest " + smallest;
    }
}
