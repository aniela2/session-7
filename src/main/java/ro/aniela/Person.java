package ro.aniela;

public class Person {
    private String name;
    private int age;
    private Gender gender;

    public Person(String name, int age, Gender gender) {
        if (gender != Gender.MALE && gender != Gender.FEMALE) {
            throw new IllegalArgumentException();
        }
        this.name = name;
        this.age = age;
        this.gender = gender;
    }

}
