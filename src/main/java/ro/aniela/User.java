package ro.aniela;

public class User {
    private Tuple name;
    private int age;
    private String city;

    public User(Tuple name, int age, String city ){
        this.name=name;
        this.age=age;
        this.city=city;
    }
    public Tuple getName(){
        return name;
    }

    public int getAge(){
        return age;
    }
    public String getCity(){
        return city;
    }
}
