package ro.aniela;

public class Wine {
    private Type type;
    private double alcoholPercentage;
    private String country;
    private double bottleSize;

    public Wine(Type type, double alcoholPercentage, String country, double bottleSize){
        this.alcoholPercentage=alcoholPercentage;
        this.country=country;
        this.type=type;
        this.bottleSize=bottleSize;
    }
    public Type getType(){
        return type;
    }
    public double getAlcoholPercentage(){
        return alcoholPercentage;
    }
    public String getCountry(){
        return country;
    }
    public double getBottleSize(){
        return bottleSize;
    }

    @Override
    public String toString() {
        return "Wine{" +
                "type=" + type +
                ", alcoholPercentage=" + alcoholPercentage +
                ", country='" + country + '\'' +
                ", bottleSize=" + bottleSize +
                '}';
    }
}
