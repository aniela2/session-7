package ro.aniela.exercise15;

import java.util.ArrayList;
import java.util.List;

public class Exercise15 {
    public static void main(String[] args) {
        List<Integer> list=List.of(1,2,3,4,5,6);
        System.out.println(oddEvenListNumbers(list));

    }

    private static Pair oddEvenListNumbers(List<Integer> list) {
        List<Integer> odd = new ArrayList<>();
        List<Integer> even = new ArrayList<>();
        for (Integer i : list) {
            if (i % 2 == 1) {
                odd.add(i);
            } else {
                even.add(i);
            }
        }
        return new Pair(odd, even);
    }
}
