package ro.aniela.exercise15;

import java.util.List;

public class Pair<V,L> {
    private V odd;
    private L even;


    public Pair(V odd, L even) {
        this.odd = odd;
        this.even = even;
    }

    public V getOdd() {
        return odd;
    }

    public L getEven() {
        return even;
    }

    @Override
    public String toString() {
        return "Pair has: odd" + odd + ", even" + even;
    }
}
