package ro.aniela.exercise16;

import java.security.PublicKey;

public class Address {
    private String street;
    private int streetNumber;
    private String town;
    private Country country;

    public Address(String street, int streetNumber, String town, Country country) {
        this.street = street;
        this.streetNumber = streetNumber;
        this.town = town;
        this.country = country;
    }

    public String getStreet() {
        return street;
    }

    public int getStreetNumber() {
        return streetNumber;
    }

    public String getTown() {
        return town;
    }

    public Country getCountry() {
        return country;
    }

}
