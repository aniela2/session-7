package ro.aniela.exercise16;

import ro.aniela.exercise15.Pair;

public final class Constants {
    public static final int EUROPEAN_ADULT_AGE=18;
    public static final int AMERICAN_LICENSE_AGE=16;
    public static final int ALL_CONTINENTS=15;
    private Constants(){
        throw new AssertionError();
    }

}
