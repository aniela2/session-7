package ro.aniela.exercise16;

public enum Country {
    ROMANIA("Europa"),
    BULGARIA("Europa"),
    INDONEZIA ("ASIA");

    private final String continent;


    private Country(String continent) {
        this.continent = continent;
    }

    public String getContinent() {
        return continent;
    }
}
