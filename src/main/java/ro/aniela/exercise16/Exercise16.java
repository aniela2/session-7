
package ro.aniela.exercise16;

import java.util.Set;

public class Exercise16 {
    public static void main(String[] args) {


        Name person = new Name("Iana", "Georgescu");
        Name person2 = new Name("Ion", "La");
        Name person3 = new Name("Oana", "Mara");
        Address adresa1 = new Address("jsf", 12, "town", Country.BULGARIA);
        Address adresa2 = new Address("str", 16, "nb", Country.INDONEZIA);
        Address adresa3 = new Address("shf", 15, "town", Country.ROMANIA);

        Individual ind = new Individual(person, 18, Gender.FEMALE, adresa1);
        Individual indd = new Individual(person2, 23, Gender.MALE, adresa2);
        Individual ind4 = new Individual(person3, 27, Gender.FEMALE, adresa3);
        Set<Individual> individual = Set.of(ind, indd, ind4);
        namesUppercase(individual);
    }

    private static void namesUppercase(Set<Individual> individuals) {

        for (Individual i : individuals) {
            if (i.getAge() >= Constants.EUROPEAN_ADULT_AGE || i.getAge() >= Constants.AMERICAN_LICENSE_AGE || i.getAge() >= Constants.ALL_CONTINENTS) {
                System.out.println((i.getName().getFirst() + " "+i.getName().getLast()).toUpperCase());
            }
        }

    }
}