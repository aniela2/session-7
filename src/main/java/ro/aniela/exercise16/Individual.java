package ro.aniela.exercise16;

public class Individual {
    private Name name;
    private int age;
    private Gender gender;
    private Address adress;

    public Individual(Name name, int age, Gender gender, Address address) {
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.adress = address;
    }

    public Name getName() {
        return name;

    }

    public int getAge() {
        return age;
    }

    public Gender getGender() {
        return gender;
    }

    public Address getAdress() {
        return adress;
    }
}
