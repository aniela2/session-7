package ro.aniela.exercise17;

import java.time.LocalDate;
import java.util.Objects;

public class Actor {
    private String name;
    private Gender gender;
    private LocalDate dateOfBirth;

    public Actor(String name, Gender gender, LocalDate dateOfBirth) {

        this.name = Objects.requireNonNull(name);
        this.gender = gender;
        this.dateOfBirth = dateOfBirth;
    }

    public String getName() {
        return name;
    }

    public Gender getGender() {
        return gender;

    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Actor actor = (Actor) o;
        return name.equals(actor.name) && gender == actor.gender && Objects.equals(dateOfBirth, actor.dateOfBirth);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, gender, dateOfBirth);
    }

    @Override
    public String toString() {
        return "Actor : name " + name + ", gender" + gender + ", dateOfBirth" + dateOfBirth;
    }
}
