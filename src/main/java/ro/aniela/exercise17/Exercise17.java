package ro.aniela.exercise17;

import java.time.LocalDate;
import java.util.*;

public class Exercise17 {

    public static void main(String[] args) {
        Actor two2 = new Actor("Adrian", Gender.MALE, LocalDate.of(1984, 06, 20));
        Actor one1 = new Actor("Ioana", Gender.FEMALE, LocalDate.of(1991, 02, 22));

        List<Actor> actors = List.of(one1, two2);
        Actor b = new Actor("O", Gender.FEMALE, LocalDate.of(1991, 05, 01));
        Actor a = new Actor("OAna ", Gender.FEMALE, LocalDate.of(1992, 10, 06));
        List<Actor> actors2 = List.of(a, b);

        Movie one = new Movie("name", 8.5, 1996, MovieType.ACTION, actors);
        Movie two = new Movie("two", 9, 2010, MovieType.LOVE, actors);
        Movie three = new Movie("three", 10, 2014, MovieType.HORROR, actors2);

        Set<Movie> movies = Set.of(one, two, three);

        System.out.println(typeOfMovie(MovieType.ACTION, movies));
        System.out.println(countActorsByGender(movies, Gender.FEMALE));
        System.out.println(retrieveActors(movies));
        System.out.println(sumOfImdb(movies));
        System.out.println(movieTypes(movies));
        System.out.println(bornIn21(movies));
        System.out.println(theBestRating(movies));
    }

    private static List<Movie> typeOfMovie(MovieType movieType, Set<Movie> moviess) {
        List<Movie> movies = new ArrayList<>();

        for (Movie m : moviess) {
            if (m.getMovieType() == movieType) {
                movies.add(m);
            }

        }
        return movies;
    }

    private static int countActorsByGender(Set<Movie> movies, Gender g) {
        int sum = 0;
        for (Movie m : movies) {
            for (Actor a : m.getActors()) {

                if (a.getGender() == g) {
                    sum++;
                }
            }

        }
        return sum;
    }

    private static List<Actor> retrieveActors(Set<Movie> movies) {
        List<Actor> a = new ArrayList<>();
        for (Movie mm : movies) {
            a.addAll(mm.getActors());
        }
        return a;
    }

    private static double sumOfImdb(Set<Movie> movie) {
        double sum = 0.0;
        for (Movie m : movie) {
            sum += m.getImdbRate();
        }
        return sum;
    }

    private static Set<MovieType> movieTypes(Set<Movie> movies) {
        Set<MovieType> mtt = new HashSet<>();
        for (Movie m : movies) {
            mtt.add(m.getMovieType());
        }
        return mtt;
    }

    private static List<Actor> bornIn21(Set<Movie> movie) {
        List<Actor> actors = new ArrayList<>();
        for (Movie m : movie) {
            for (Actor a : m.getActors()) {
                if (a.getDateOfBirth().getYear() >= 2001 && a.getDateOfBirth().getYear() <= 2100) {
                    actors.add(a);
                }

            }

        }
        return actors;
    }

    private static double theBestRating(Set<Movie> movies) {
        double raiting=0.0;
        for(Movie m:movies){
            if(raiting <m.getImdbRate()){
                raiting=m.getImdbRate();
            }

        }
        return raiting;
    }
}

