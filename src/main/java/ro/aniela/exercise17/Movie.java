package ro.aniela.exercise17;

import java.util.List;
import java.util.Objects;

public class Movie {

    private String name;
    private Integer releaseYear;
    private double imdbRate;
    private MovieType movieType;
    private List<Actor> actors;

    public Movie(String name, double imdbRate, Integer releaseYear, MovieType movieType, List<Actor> actors) {
        if (Objects.isNull(actors) && actors.isEmpty()) {
            throw new IllegalArgumentException();
        }
        this.actors = actors;
        this.releaseYear = Objects.requireNonNull(releaseYear);
        this.name = Objects.requireNonNull(name);
        this.imdbRate = imdbRate;
        this.movieType = movieType;

    }

    public String getName() {
        return name;
    }

    public double getImdbRate() {
        return imdbRate;
    }

    public Integer getReleaseYear() {
        return releaseYear;
    }

    public MovieType getMovieType() {
        return movieType;
    }

    public List<Actor> getActors() {
        return actors;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Movie movie = (Movie) o;
        return Double.compare(movie.imdbRate, imdbRate) == 0 && name.equals(movie.name) && releaseYear.equals(movie.releaseYear) && movieType == movie.movieType && actors.equals(movie.actors);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, releaseYear, imdbRate, movieType, actors);
    }

    @Override
    public String toString() {
        return "Movie: name" + name + ", ImdbRate" + imdbRate + ", MovieType" + movieType + ", actors" + actors;
    }


}
