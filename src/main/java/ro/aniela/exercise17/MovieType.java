package ro.aniela.exercise17;

public enum MovieType {
    HORROR, ACTION, LOVE, THRILLER;
}
