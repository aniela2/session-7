package ro.aniela.exercise20;

import java.time.LocalDate;
import java.util.Locale;

public class Author {
    private String name;
    private String alias;
    private LocalDate dateOfDebut;

    public Author (String name, String alias, LocalDate dateOfDebut){
        this.alias=alias;
        this.name=name;
        this.dateOfDebut=dateOfDebut;
    }
    public String getName(){
        return name;
    }
    public String getAlias(){
        return alias;
    }
    public LocalDate getDateOfDebut(){
        return dateOfDebut;
    }
}
