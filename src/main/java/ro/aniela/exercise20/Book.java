package ro.aniela.exercise20;

public class Book {
    private String name;
    private Author author;
    private int releaseYear;
    private BookCategory bookCategory;

    public Book(String name, Author author, int releaseYear, BookCategory bookCategory) {
        this.author = author;
        this.name = name;
        this.releaseYear = releaseYear;
        this.bookCategory = bookCategory;

    }

    public String getName() {
        return name;
    }

    public Author getAuthor() {
        return author;
    }

    public int getReleaseYear() {
        return releaseYear;
    }

    public BookCategory getBookCategory() {
        return bookCategory;
    }
}
