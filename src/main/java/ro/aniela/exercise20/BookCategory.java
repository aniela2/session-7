package ro.aniela.exercise20;

public enum BookCategory {
    NOVEL, ROMANCE, SF;
}
