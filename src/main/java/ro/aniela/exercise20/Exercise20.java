package ro.aniela.exercise20;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Exercise20 {
    public static void main(String[] args) {
        Author a = new Author("ana", "a", LocalDate.of(1987, 02, 22));
        Author bb = new Author("anabela", "bella", LocalDate.of(2000, 03, 12));
        Author cc = new Author("Man", "Aqua", LocalDate.of(2010, 05, 12));
        Book b = new Book("Book", a, 1980, BookCategory.ROMANCE);
        Book c = new Book("Harry", bb, 1999, BookCategory.SF);
        Book d = new Book("Journal", cc, 2000, BookCategory.ROMANCE);
        List<Book> books = List.of(b, c, d);
        System.out.println(authors(books));
        System.out.println(booksCategories(books));
        System.out.println(booksByCategory(books));
        System.out.println(bookForEachYear(books));

    }

    private static List<Author> authors(List<Book> books) {
        List<Author> a = new ArrayList<>();
        for (Book b : books) {
            a.add(b.getAuthor());

        }

        return a;
    }

    private static List<BookCategory> booksCategories(List<Book> books) {
        List<BookCategory> b = new ArrayList<>();
        for (Book book : books) {
            b.add(book.getBookCategory());
        }
        return b;
    }

    private static Map<BookCategory, List<Book>> booksByCategory(List<Book> books) {
        Map<BookCategory, List<Book>> map = new HashMap<>();
        for (Book b : books) {
            if (map.containsKey(b.getBookCategory())) {
                map.get(b.getBookCategory()).add(b);
            } else {
                List<Book> listOfBooks = new ArrayList<>();
                listOfBooks.add(b);
                map.put(b.getBookCategory(), listOfBooks);
            }
        }
        return map;
    }

    private static Map<Integer, List<Book>> bookForEachYear(List<Book> books) {
        Map<Integer, List<Book>> map = new HashMap<>();
        for (Book b : books) {
            if (map.containsKey(b.getReleaseYear())) {
                map.get(b.getReleaseYear()).add(b);
            }
            else

            {
                List<Book> boooks = new ArrayList<>();
                boooks.add(b);
                map.put(b.getReleaseYear(), boooks);

            }
        }
        return map;
    }
}